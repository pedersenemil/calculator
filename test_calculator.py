from calculator import add, subtract


def test_add():
    result = add(2, 2)
    assert result == 4


def test_subtract():
    result = subtract(2, 2)
    assert result == 0
